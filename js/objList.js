const divListSet = document.getElementById('listSet')

class List {
    constructor (titleList, idList) {
        this.title  = titleList
        this.idList = idList
    }

    render() {
        // HEADER LIST
        let title = document.createElement('h1')
        title.textContent = this.title
        
        let divDeleteList = document.createElement('div')
        divDeleteList.classList.add('deleteList')
        divDeleteList.addEventListener('click', deleteList)
        divDeleteList.innerHTML = '<i class="fas fa-trash-alt"></i>'

        let divHeader = document.createElement('div')
        divHeader.classList.add('listHeader')
        divHeader.appendChild(title)
        divHeader.appendChild(divDeleteList)

        // BOX CARD
        let divCardContent = document.createElement('div')
        divCardContent.classList.add('cardContent')

        // BOX DIV ADD CARD
        let spanTitleAddCard = document.createElement('span') 
        spanTitleAddCard.innerHTML = '<i class="fa fa-plus" aria-hidden="true" style="font-size: 0.8rem;"></i>&nbsp; Adicionar um cartão'

        let divAddCard = document.createElement('div')
        divAddCard.classList.add('addCard')
        divAddCard.addEventListener('click', createCard)
        divAddCard.appendChild(spanTitleAddCard)

        let textareaAddCard = document.createElement('textarea')
        textareaAddCard.classList.add('titleCardForm')
        textareaAddCard.placeholder = 'Insira o título para este cartão...'

        let inputAddCard = document.createElement('div')
        inputAddCard.classList.add('inputAddCard')
        inputAddCard.innerHTML = '<input type="button" value="Adicionar cartão"><i class="closeFormCard">&times;</i>'


        let divAddCardForm = document.createElement('div')
        divAddCardForm.classList.add('addCardForm')
        divAddCardForm.appendChild(textareaAddCard)
        divAddCardForm.appendChild(inputAddCard)
        

        // DIV LIST CONTENT
        let divListContent = document.createElement('div')
        divListContent.classList.add('listContent')
        divListContent.id = `list-${this.idList}`
        divListContent.appendChild(divHeader)
        divListContent.appendChild(divCardContent)
        divListContent.appendChild(divAddCard)
        divListContent.appendChild(divAddCardForm)

        let childLast = document.getElementById('addList')
        divListSet.insertBefore(divListContent, childLast)
    }
}