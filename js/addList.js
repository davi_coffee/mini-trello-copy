const SET_LIST_AND_INDEX = {
    index_list: 0,
}


const inputAddList = document.getElementById('addList')
inputAddList.addEventListener('click', visibleFormList)

const addListForm = document.getElementById('addListForm')

const titleListForm = document.getElementById('titleListForm')

function visibleFormList() {
    inputAddList.style.display = 'none'
    addListForm.style.display = 'block'
    titleListForm.value = ''
}

function visibleDivAddList(){
    inputAddList.style.display = 'block'
    addListForm.style.display = 'none'
    titleListForm.value = ''
}

const closeFormList = document.getElementById('closeFormList')
closeFormList.addEventListener('click', visibleDivAddList)

const inputSaveList = document.getElementById('saveList')
inputSaveList.addEventListener('click', createList)
function createList(){
    let textTitle = titleListForm.value.trim()

    visibleDivAddList()

    if (textTitle) {
       let newList = new List(textTitle,SET_LIST_AND_INDEX.index_list)
       SET_LIST_AND_INDEX.index_list++
       newList.render()
    }
}

