const CARD_INDEX = {
    index_card: 0,
}

function visibleFormCard() {
    inputAddCard.style.display = 'none'
    addListCard.style.display = 'block'
    titleCardForm.value = ''
}

function visibleDivAddCard() {
    inputAddCard.style.display = 'block'
    addListCard.style.display = 'none'
    titleCardForm.value = ''
}
let listContentFather, inputAddCard, addListCard,titleCardForm 

function createCard(evt) {
    listContentFather = evt.currentTarget.parentElement
    inputAddCard = listContentFather.children[2]
    addListCard = listContentFather.children[3]
    titleCardForm = addListCard.children[0]

    console.log(listContentFather)

    visibleFormCard()

    // span cancel list
    let cancelCardSpan = addListCard.children[1].children[1]
    cancelCardSpan.addEventListener('click', visibleDivAddCard)

    // button Save Card
    let inputSaveCard = addListCard.children[1].children[0]
    inputSaveCard.addEventListener('click', constructorCard)
    
}

function constructorCard() {
    if (titleCardForm.value.trim()) {
        let titleCard = titleCardForm.value.trim()
        let newCard = new Card(titleCard, listContentFather, CARD_INDEX.index_card)
        CARD_INDEX.index_card++
        
        newCard.render()
    }

    visibleDivAddCard()
}