class Card {
    constructor(title, divFatherList, idCard) {
        this.title = title
        this.cardContent = divFatherList.children[1]
        this.idCard = idCard
    }

    render() {
        let titleCard = document.createElement('h2')
        titleCard.textContent = this.title

        let header = document.createElement('header')
        header.appendChild(titleCard)
        
        let divCard = document.createElement('div')
        divCard.classList.add('card')
        divCard.id = `card-${this.idCard}`
        divCard.appendChild(header)

        this.cardContent.appendChild(divCard)
    }
}